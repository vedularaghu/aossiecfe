const fetch = require('cross-fetch');
const DEFAULT_SERVER_URL = "https://carbonhub.xyz";
const DEFAULT_VERSION = 1;
class Client {
    constructor(apiKey, {
        serverUrl = DEFAULT_SERVER_URL,
        version = DEFAULT_VERSION
    } = {}) {
        this.baseUrl =`${serverUrl}/v${version}`;
        this.apiKey = apiKey;
    }

    __makeRequest(endPoint, data) {
        return fetch(`${this.baseUrl}/${endPoint}`, {
            method: 'POST',
            headers: {
                'access-key': this.apiKey,
                'Content-type': 'application/json'
            },
            body: JSON.stringify(data)
        }).then(res => res.json())
    }

    appliances(data) {
        return this.__makeRequest('appliances', data);
    }

    emissions(data) {
        return this.__makeRequest('emissions', data);
    }

    poultry(data) {
        return this.__makeRequest('poultry', data);
    }

    quantity(data) {
        return this.__makeRequest('quantity', data);
    }

    flight(data) {
        return this.__makeRequest('flight', data);
    }

    vehicle(data) {
        return this.__makeRequest('vehicle', data);
    }

    trains(data) {
        return this.__makeRequest('trains', data);
    }
}

function client(clientId, params) {
    return new Client(clientId, params);
}

module.exports = {
    client,
    Client
};