# aossiecfe

It is an npm package which fetches data using [CarbonFootprint-API](https://gitlab.com/aossie/CarbonFootprint-API).

# Install

```
npm install https://gitlab.com/vedularaghu/aossiecfe

```

# Available API EndPoints 

| APIEndPoints  | Uses                         | Supported Items (If any) |
| ------------- |:-------------:               | :-----------------------:|
| appliances    | GHG Emissions for appliances | None
| emisssions      | GHG Emissions for things   | Electricity, Airplane Fuel, Vehicle Fuel, Trains, Trees, Appliances |
| poultry | GHG emissions for different types of Poultry meat production | None       |
| quantity | Used to retrieve the quantity of a certain element provided the CO2 emission for the specific item is already known| None |
| flights | Emissions for a flight between two airports | None |
| vehicle | GHG emissions for a number of fuels. | None |
| trains  | GHG emissions for a number of train types for a certain route | None |

[Check](https://docs.carbonhub.xyz/) this for futher information.

# Usage

```
const api = require('aossiecfe');

const client = api.client("API Key provided in the carbonhub.xyz", (optional){url: "https://carbonhub.xyz is default. You can change it to localhost", version:"Default is 1 as our API has only one version"});

client.apiEndPoint("Data in JSON format whose emission you want").then(console.log);

```

# Sample Request 
 
 ```
 const api = require('aossiecfe);
 
 const client = api.client("b8XXXXXXXXXXX");
 
 client.trains({
    "type":"railcars",
    "origin":"Bhubaneswar",
    "destination":"Delhi",
    "passengers":10
}).then(console.log);
 ```
 ```
 { success: true, emissions: { CO2: 748.5628 }, unit: 'kg' }

 ```

Licenses
---------
* GNU-GPL-3.0

* CC-By-NC-ND [![License](https://i.creativecommons.org/l/by-nc-nd/4.0/88x31.png)](http://creativecommons.org/licenses/by-nc-nd/4.0/)